import os
from tkinter import filedialog
from tkinter import *

def chooseDir():
    chosenDir = filedialog.askdirectory()
    files =  0
    folders = 0
    for _, dirnames, filenames in os.walk(chosenDir): # os.walk gibt ein 3-Tupel zurück (dirpath, dirnames, filenames). _ bedeutet, dass der erste Wert nicht benutzt wird
        files += len(filenames)
        folders += len(dirnames)
    lCountDirs = Label(gui, text="Directories counted: " + str(folders))
    lCountDirs.pack()
    lCountFiles = Label(gui, text="Files counted: " + str(files))
    lCountFiles.pack()
    lChosenDir = Label(gui, text="Chosen Directory: " + chosenDir)
    lChosenDir.pack()

gui = Tk(className="File Counter") # Mainwidget (Fenster)
gui.geometry('400x250') # Größe des Fensters
bDir = Button(text="Choose Dir and count", command = chooseDir)
bDir.pack()

gui.mainloop()